Feature:Test app Banggood

  Scenario Outline: Test case 1
    When User click to the Category on footer menu
    And Scroll and click to Home and Garden in Left menu
    And After the right category displayed, click to the “Home Decor”
    And Input price from <valueMin> to <valueMax> for filter
    And User click first product
    Then User should see product name displayed and product price in range <valueMin> to <valueMax>
    Examples:
      | valueMin | valueMax |
      | 20       | 30       |

  Scenario: Test case 2
    When User scroll to Hot Categories at Home screen and click to first category
    And User click first product
    And After product detail displayed , user click Add to cart
    And User click to Cart icon on header
    Then User should see product name, product size, product price and quantity

  Scenario: Test case 3
    When User click Account on footer menu
    And User click View All Order
    Then The login screen should be displayed with: Email, password and Sign In button

#  Scenario: Test case 4
#    When User scroll to Hot Categories at Home screen and click to first category
#    And User click first product
#    And After product detail displayed , user click Buy now
#    Then User should see product name, product size, product price and quantity
