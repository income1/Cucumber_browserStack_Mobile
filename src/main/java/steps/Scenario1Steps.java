package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import keywords.AndroidKeywords;
import locators.Locators;
import org.testng.Assert;

import java.net.MalformedURLException;

public class Scenario1Steps {
    AndroidKeywords keywords = new AndroidKeywords();
    Locators locators = new Locators();

    @Given("App open successfully")
    public void openApp() throws MalformedURLException {
    }

    @When("User click to the Category on footer menu")
    public void clickToCategory(){

        keywords.waitUntilPageContainsElement(locators.category);
        keywords.clickElementByXpath(locators.category);
    }

    @And("Scroll and click to Home and Garden in Left menu")
    public void scrollAndClickToHomeAndGarden(){
        keywords.scrollToElement(locators.leftMenu,locators.homeAndGarden);
        keywords.clickElementByXpath(locators.homeAndGarden);
    }

    @And("After the right category displayed, click to the “Home Decor”")
    public void clickToDecor(){
        keywords.waitUntilPageContainsElement(locators.homeDecor);
        keywords.scrollToElement(locators.rightMenu,locators.homeDecor);
        keywords.clickElementByXpath(locators.homeDecor);
    }

    @And("Input price from {int} to {int} for filter")
    public void setFilterValue(int minValue,int maxValue) throws InterruptedException {
        keywords.waitToClickAble(locators.filterButton, 20);
        keywords.clickElementByXpath(locators.filterButton);
        keywords.sendKeyByXpath(locators.minPriceEdit,String.valueOf(minValue));
        keywords.sendKeyByXpath(locators.maxPriceEdit,String.valueOf(maxValue));
        keywords.clickElementByXpath(locators.doneButton);
    }

    @And("User click first product")
    public void clickToFirstProduct(){
        keywords.waitUntilPageContainsElement(locators.firstProduct);
        keywords.clickElementByXpath(locators.firstProduct);
    }

    @Then("User should see product name displayed and product price in range {int} to {int}")
    public void verifyNameAndPrice(int minPrice,int maxPrice){
        keywords.waitUntilPageContainsElement(locators.productName);
        keywords.verifyElementByXpath(locators.productName);
        Assert.assertTrue(keywords.verifyProductPrice(locators.productPrice, minPrice,maxPrice));
        keywords.goBack();
        keywords.goBack();
        keywords.clickElementByXpath(locators.homePage);
    }
}
